FROM ubuntu:16.04

MAINTAINER Your Name "vadim.dobrovolsky@gmail.com"

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev git

RUN git clone https://gitlab.com/vdabravolski/samplepythonapi.git /src/samplepythonapi

WORKDIR /src/samplepythonapi

RUN pip install -r requirements.txt

ENTRYPOINT [ "python" ]

CMD [ "server.py" ]